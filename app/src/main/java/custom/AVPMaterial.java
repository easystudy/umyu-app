package custom;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.ProcessingInstruction;

/**
 * Created by swifta on 7/5/17.
 */

public class AVPMaterial {

    private String id, aid, pid, vid, title, fileName, fileUrl, courseMaterialId, userId;
    private JSONObject jsonObject;
    private String j_aid, j_pid, j_title, j_fileName, j_fileUrl, j_courseMaterialId, j_userId;

    public void setId(String id) {
        this.id = id;
    }
    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getCourseMaterialId() {
        return courseMaterialId;
    }

    public void setCourseMaterialId(String courseMaterialId) {
        this.courseMaterialId = courseMaterialId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getId() throws JSONException{
        return getJsonObject().getString(Constants.KEY_ID);
    }


    public String getJ_title() throws JSONException{
        return getJsonObject().getString(Constants.KEY_AVP_TITLE);
    }

    public String getJ_fileName() throws JSONException{
        return getJsonObject().getString(Constants.KEY_AVP_FILE_NAME);
    }

    public String getJ_fileUrl() throws JSONException{
        return getJsonObject().getString(Constants.KEY_AVP_FILE_URL);
    }

    public String getJ_courseMaterialId() throws JSONException{
        return getJsonObject().getString(Constants.KEY_AVP_COURSE_MATERIALS_ID);
    }

    public String getJ_userId() throws JSONException{
        return getJsonObject().getString(Constants.KEY_AVP_USERS_ID);
    }
}

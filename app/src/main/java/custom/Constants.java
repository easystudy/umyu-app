package custom;

public final class Constants {
    public static String filepath = "MyFileStorage";
    public static final String downloadUrl = "http://umyu.easystudy.com.ng/materials/";
    public static final int CLASS_CONSTANT_VALUE = 5;
    public static final String IMAGE = "image";
    public static final String TEXT = "text";
    public static final String KEY_BODY = "body";
    public static final String KEY_TYPE = "type";
    public static final String KEY_COURSE_ID = "course_id";
    //Server API Key help
    // AIzaSyApWltkjGKvtoy8PXwu8O_0Z2ghYOt4q1A
    public static final String KEY_AID = "aid";
    public static final String KEY_VID = "vid";
    public static final String KEY_PID = "pid";
    public static final String KEY_AVP_TITLE = "title";
    public static final String KEY_AVP_FILE_NAME = "file_name";
    public static final String KEY_AVP_FILE_URL = "file_url";
    public static final String KEY_AVP_COURSE_MATERIALS_ID = "course_materials_id";
    public static final String KEY_AVP_COURSE_CODES_ID = "course_codes_id";
    public static final String KEY_AVP_USERS_ID = "users_id";

    public static final String VIDEO_DIRECTORY = ".UMYU/.data/.materials/.videos";
    public static final String AUDIO_DIRECTORY = ".UMYU/.data/.materials/.audios";
    public static final String PDF_DIRECTORY = ".UMYU/.data/.materials/.pdfs";
    public static final String POWERPOINT_DIRECTORY = ".UMYU/.data/.materials/.powerpoints";


    public static final String KEY_ID = "id";
    public static final String KEY_CID = "cid";
    public static final String KEY_AUTO_ID = "id";
    public static final String COURSE_MATERIALS = "course_materials";
    public static final String DB_NAME = "easy_study";
    public static final String TABLE_NAME = "table_name";
    public static final String TABLE_AUDIO = "audio_materials";
    public static final String TABLE_VIDEO = "video_materials";
    public static final String TABLE_POWER_POINT = "power_point_materials";

    public static final String TABLE_COURSE_CODE = "course_codes";
    public static final String KEY_CODE = "code";
    public static final String AUDIO = "Audio";
    public static final String VIDEO = "Video";
    public static final String POWER_POINT = "Powerpoint";
    public static final String PDF = "Pdf";
    public static final String PDFPATH = "pdf/";
    public static final String AUDIOPATH = "audio/";
    public static final String VIDEOPATH = "video/";
    public static final String FILE_NAME = "file_name";
    public static final String PDF_FILE = "pdffile";
    public static final String AUDIO_FILE = "audiofile";
    public static final String VIDEO_FILE = "videofile";
    public static final String KEY_TITLE = "title";
    public static final String KEY_MATERIAL_TYPE = "material_type";
    //constants for pubnub group chat
    public static final String PUBLISH_KEY = "pub-c-cbad7f22-79c1-4606-84a9-8c28a4e79851";
    public static final String SUBSCRIBE_KEY = "sub-c-04b492c8-65de-11e7-9bf2-0619f8945a4f";

    public static final String CHAT_PREFS = "SHARED_PREFS";
    public static final String CHAT_USERNAME = "SHARED_PREFS.USERNAME";
    public static final String ACCESS_PIN = "SHARED_PREFS.ACCESS_PIN";
    public static final String CHAT_ROOM = "CHAT_ROOM";

    public static final String JSON_GROUP = "groupMessage";
    public static final String JSON_DM = "directMessage";
    public static final String JSON_USER = "chatUser";
    public static final String JSON_MSG = "chatMsg";
    public static final String JSON_TIME = "chatTime";

    public static final String STATE_LOGIN = "loginTime";

    public static final String GCM_REG_ID = "gcmRegId";
    public static final String GCM_SENDER_ID = "221425343743"; // Get this from
    public static final String GCM_POKE_FROM = "gcmPokeFrom"; // Get this from
    public static final String GCM_CHAT_ROOM = "gcmChatRoom"; // Get this from
    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static final String ACTIVITY_NAME = "activity_name"; // Get activity name


    //API constants

    public static final String CODES_JSON_RESPONSE = "Codes";
    public static final String MATERIALS_JSON_RESPONSE = "Materials";

    public static final String USER_NAME = "name";
    public static final String USER_PIN = "pin";
    public static final String APP_USER_JSON_RESPONSE = "AppUser";
    public static final String MESSAGE = "message";
    public static final String STATUS = "status";

    //urls
    static final String SERVICE_URL = "http://umyu.easystudy.com.ng/";
    static final String SERVICE_ENDPOINT = SERVICE_URL + "api/v1/api/v1";
    public static final String COURSE_CODES_ENDPOINT = SERVICE_ENDPOINT + "/course/code";
    public static final String USER_ACTIVATION_ENDPOINT = SERVICE_ENDPOINT + "/user/activate";

    public static final String COURSE_MATERIALS_ENDPOINT = SERVICE_ENDPOINT + "/course/material/";

}
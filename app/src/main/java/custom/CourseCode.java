package custom;

import org.json.JSONObject;

/**
 * Created by HighStrit on 03/06/2017.
 */

public class CourseCode {
    private String code, title;
    private JSONObject jsonObject;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}

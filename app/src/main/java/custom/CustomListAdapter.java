package custom;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import com.cbc.umyu.R;
/**
 * Created by HighStrit on 18/03/2017.
 */
public class CustomListAdapter extends ArrayAdapter<Material> {
    private final Activity context;
    private ArrayList<Material> materials;

    public CustomListAdapter(Activity context, ArrayList<Material> materials) {
        // TODO Auto-generated constructor stub
        super(context, R.layout.spinner_list, materials);
        this.context=context;
        this.materials = materials;
    }

    @SuppressLint({ "InflateParams", "ViewHolder" })
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.spinner_list, null, true);
//        TextView title = (TextView) rowView.findViewById(R.id.title);
//        TextView body = (TextView) rowView.findViewById(R.id.body);
//        ImageView image = (ImageView) rowView.findViewById(R.id.image);
//        if(materials.get(position).getTitle().equals(Constants.IMAGE)){
//            title.setVisibility(View.GONE);
//            body.setVisibility(View.GONE);
//            image.setVisibility(View.VISIBLE);
//            String name =v materials.get(position).getBody();
//            int id = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
//            image.setImageResource(id);
//        }
//        else{
//            title.setVisibility(View.VISIBLE);
//            body.setVisibility(View.VISIBLE);
//            title.setText(Html.fromHtml(materials.get(position).getTitle()));
//            body.setText(Html.fromHtml(materials.get(position).getBody()));
//        }
        return rowView;
    }
}

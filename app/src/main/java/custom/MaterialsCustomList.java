package custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cbc.umyu.R;

import java.util.ArrayList;

public class MaterialsCustomList extends BaseAdapter {
	Context context;
	ArrayList<AVPMaterial> items;
	LayoutInflater inflter;

	public MaterialsCustomList(Context applicationContext, ArrayList<AVPMaterial> items) {
		this.context = applicationContext;
		this.items = items;
		inflter = (LayoutInflater.from(applicationContext));
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int i) {
		return null;
	}

	@Override
	public long getItemId(int i) {
		return 0;
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		view = inflter.inflate(R.layout.avp_list, null);
		TextView id = (TextView) view.findViewById(R.id.id);
		TextView title = (TextView) view.findViewById(R.id.title);
		TextView file_url = (TextView) view.findViewById(R.id.file_url);
		TextView file_name = (TextView) view.findViewById(R.id.file_name);
		TextView course_materials_id = (TextView) view.findViewById(R.id.course_materials_id);
		course_materials_id.setText(items.get(i).getCourseMaterialId());
		file_url.setText(items.get(i).getFileUrl());
		title.setText(items.get(i).getTitle());
		file_name.setText(items.get(i).getFileName());
		return view;
	}
}
package custom;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

import com.cbc.umyu.FacultyActivity;
import com.cbc.umyu.R;
public class FacultyList extends BaseAdapter {
    Context context;
    ArrayList<String> items;
    LayoutInflater inflter;

    public FacultyList(Context applicationContext, ArrayList<String> items) {
        this.context = applicationContext;
        this.items = items;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spin, null);
        TextView code =  view.findViewById(R.id.code);
        code.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.title_gray, null));
        code.setText(items.get(i));
       // title.setText(items.get(i).getTitle());
       // id.setText(items.get(i).getId());
        return view;
    }
}
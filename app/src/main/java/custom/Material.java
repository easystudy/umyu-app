package custom;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HighStrit on 12/05/2017.
 */
public class Material {
    private String id, title, body, type, code,level;
    private JSONObject jsonObject;

    public Material(){
        this.title="";
        this.body="";
        this.type ="";
        this.code = "";

    }
    public Material(String code, String title){
        this.title = title;
        this.code = code;
    }
    public Material(String id, String code, String title){
        this.id = id;
        this.title = title;
        this.code = code;
    }
    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode(){
       return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getJId() throws JSONException{
        return getJsonObject().getString((Constants.KEY_ID));
    }

    public String getJTitle() throws  JSONException{
        return getJsonObject().getString(Constants.KEY_TITLE);
    }

    public String getJCode() throws JSONException{
        return getJsonObject().getString(Constants.KEY_CODE);
    }
}

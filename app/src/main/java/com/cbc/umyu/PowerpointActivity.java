package com.cbc.umyu;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import custom.Constants;

public class PowerpointActivity extends AppCompatActivity{
    private static final String TAG = MainActivity.class.getSimpleName();
    private File myInternalFile, myCachedFile, directory;
    private String uriPath;
    private Uri UrlPath = null;
    private Button status_button, viewPPt;

    private String fileUrl;
    private LinearLayout errorView;
    private TextView errorText;

    private ImageView powerPointView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_powerpoint);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        toolbar.setTitle(getIntent().getStringExtra(Constants.KEY_AVP_TITLE));
        toolbar.setNavigationIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.back_icon, null));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.inflateMenu(R.menu.main);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                //noinspection SimplifiableIfStatement
                if (id == R.id.update) {
                    if(isConnected()) {
                        new DownloadingTask().execute();
                    }
                    else{
                        status_button.setVisibility(View.VISIBLE);
                        status_button.setText("Enable internet to update course material");
                    }
                    return true;
                }
                return onMenuItemClick(item);
            }
        });


        status_button =(Button) findViewById(R.id.status_button);
        errorView = (LinearLayout) findViewById(R.id.errorView);
        errorText = (TextView) findViewById(R.id.errorText);
        fileUrl = getIntent().getStringExtra(Constants.KEY_AVP_FILE_URL);
        ContextWrapper contextWrapper = new ContextWrapper(PowerpointActivity.this);
        File _directory = contextWrapper.getDir(Constants.filepath, Context.MODE_PRIVATE);
        myCachedFile = new File(_directory , getIntent().getStringExtra(Constants.KEY_AVP_FILE_NAME));

        directory =  new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), Constants.POWERPOINT_DIRECTORY);
        // Create the storage directory if it does not exist
        if (directory.exists()) {
            myInternalFile = new File(directory.getPath() + File.separator
                    + getIntent().getStringExtra(Constants.KEY_AVP_FILE_NAME));
        }
        viewPPt = (Button) findViewById(R.id.viewFile);
        powerPointView = (ImageView) findViewById(R.id.viewPowerPoint);
        viewPPt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayFile();
            }
        });
        //download file if not downloaded
        status_button = (Button) findViewById(R.id.status_button);

        //get file
        status_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isConnected()) {
                    new DownloadingTask().execute();
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
    private void displayFile() {
        try {
            if(myInternalFile != null) {
                if (myInternalFile.isFile() && myInternalFile.exists()) {
                    status_button.setVisibility(View.GONE);
                    errorView.setVisibility(View.GONE);
                    loadPowerPoint();
                } else {
                    if (isConnected()) {
                        new DownloadingTask().execute();
                    } else {
                        errorView.setVisibility(View.VISIBLE);
                        errorText.setText("Enable internet to download file");
                        status_button.setVisibility(View.VISIBLE);
                        status_button.setText("Enable internet to download file");
                    }
                }
            }
        }catch (NullPointerException ex){ex.printStackTrace();}
    }
    public void loadPowerPoint() {
        try {
            FileInputStream fis = new FileInputStream(myInternalFile);
            FileOutputStream fos = new FileOutputStream(myCachedFile);
            SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, sks);
            int blockSize = cipher.getBlockSize();
            CipherInputStream cis = new CipherInputStream(fis, cipher);
            int b;
            byte[] d = new byte[blockSize * 1024];
            while ((b = cis.read(d)) != -1) {
                fos.write(d, 0, b);
            }
            fos.close();
            cis.close();

            Uri uri = Uri.fromFile(myCachedFile);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

            myCachedFile.deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        catch (ActivityNotFoundException ex){
            Snackbar.make(viewPPt, getString(R.string.no_such_activity_found), Snackbar.LENGTH_SHORT).show();
        }
    }

    //dpownload file
    private class DownloadingTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            status_button.setEnabled(false);
            status_button.setVisibility(View.VISIBLE);
            status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.yellow, null));
            status_button.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
            status_button.setText(R.string.downloadStarted);//Set Button Text when download started
        }
        @Override
        protected void onPostExecute(Void result) {
            try {
                if (myInternalFile.exists() && myInternalFile.isFile()) {
                    status_button.setEnabled(true);
                    status_button.setVisibility(View.GONE);
                    status_button.setText(R.string.downloadCompleted);//If Download completed then change button tex
                    finish();
                    startActivity(getIntent());
                } else {
                    status_button.setText(R.string.downloadFailed);//If download failed change button text
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            status_button.setEnabled(true);
                            status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.ColorPrimaryDark, null));
                            status_button.setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
                            status_button.setVisibility(View.VISIBLE);
                            status_button.setText(R.string.downloadAgain);//Change button text again after 3sec
                        }
                    }, 3000);
                    Log.e(TAG, "Download Failed");
                }
            } catch (Exception e) {
                e.printStackTrace();

                //Change button text if exception occurs
                status_button.setText(R.string.downloadFailed);
                status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.ColorPrimaryDark, null));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        status_button.setEnabled(true);
                        status_button.setVisibility(View.VISIBLE);
                        status_button.setText(R.string.downloadAgain);
                    }
                }, 3000);
                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }
            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(getIntent().getStringExtra(Constants.KEY_AVP_FILE_URL));//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection
                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());
                }
                if (!myInternalFile.exists()) {
                    myInternalFile.createNewFile();
                    Log.e(TAG, "File Created");
                }
                FileOutputStream fos = new FileOutputStream(myInternalFile);
//                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location
                InputStream is = c.getInputStream();//Get InputStream for connection
                SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
                // Create cipher
                Cipher cipher = Cipher.getInstance("AES");
                int blockSize = cipher.getBlockSize();
                cipher.init(Cipher.ENCRYPT_MODE, sks);
                // Wrap the output stream
                CipherOutputStream cos = new CipherOutputStream(fos, cipher);
                // Write bytes
                byte[] buffer = new byte[blockSize * 1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    cos.write(buffer, 0, len1);//Write new file
                }
                // Flush and close streams.
                cos.flush();
                cos.close();
                fos.close();

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                myInternalFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }
            return null;
        }
    }

}
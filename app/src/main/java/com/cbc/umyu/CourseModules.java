package com.cbc.umyu;

import android.graphics.Bitmap;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import java.io.File;

public class CourseModules extends AppCompatActivity {

    private WebView webView;
    private ProgressBar bar;
    private File myInternalFile;
    private String file;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_modules);
        webView = (WebView) findViewById(R.id.webview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        toolbar.setTitle(getString(R.string.course_module));
        toolbar.setNavigationIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.back_icon, null));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        bar = (ProgressBar) findViewById(R.id.bar);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setWebViewClient(new Callback());
        webView.loadUrl("http://uniben-moodle.waeup.org");

    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            hideView();
        }
        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            // TODO Auto-generated method stub
            String customHtml = "<html><body><h2>Network Error!</h2></body></html>";
            webView.loadData(customHtml, "text/html", "UTF-8");
            hideView();
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showView();
        }
    }
    @Override
    public void onBackPressed() {
        //startActivity(new Intent(PowerpointActivity.this, AvpListActivity.class));
        finish();
    }
    public void showView(){
        bar.setVisibility(View.VISIBLE);
    }
    public void hideView(){
        bar.setVisibility(View.GONE);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && this.webView.canGoBack()) {
            this.webView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}

package com.cbc.umyu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import custom.Constants;
import custom.CourseCode;
import custom.Material;
import custom.CodesCustomList;
import db.DBController;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private ListView courseCodesView;
    private EditText searchCode;
    private ArrayList<Material> courseList;
    private CodesCustomList adapter;
    private ProgressBar progressBar;
    private ArrayList<CourseCode> courseCodes = new ArrayList<CourseCode>();
    private DBController db;
    private SharedPreferences mSharedPrefs;
    private ProgressDialog pDialog;
    private ImageView facebbok, twitter, youtube;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        File fileDirVideo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),Constants.VIDEO_DIRECTORY);
        File fileDirAudio = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),Constants.AUDIO_DIRECTORY);
        File fileDirPowerPoint = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), Constants.POWERPOINT_DIRECTORY);
        File fileDirPdf = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), Constants.PDF_DIRECTORY);
        if(!fileDirVideo.exists()){
            fileDirVideo.mkdirs();
        }
        if(!fileDirAudio.exists()){
            fileDirAudio.mkdirs();
        }
        if(!fileDirPowerPoint.exists()){
            fileDirPowerPoint.mkdirs();
        }
        if(!fileDirPdf.exists()){
            fileDirPdf.mkdirs();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        courseList = new ArrayList<Material>();
        //get course codes
        searchCode = (EditText) findViewById(R.id.search_code);
        courseCodesView = (ListView) findViewById(R.id.course_codes);
        facebbok = (ImageView) findViewById(R.id.facebook);
        twitter = (ImageView) findViewById(R.id.twitter);
        youtube = (ImageView) findViewById(R.id.youtube);

        mSharedPrefs = getSharedPreferences(Constants.CHAT_PREFS, MODE_PRIVATE);
        pDialog = new ProgressDialog(MainActivity.this);
        db = new DBController(this);
        courseList = db.getCourseCodes();
        adapter = new CodesCustomList(MainActivity.this, courseList);
        courseCodesView.setAdapter(adapter);
        if(isConnected()){
            new CourseCodeTask().execute(Constants.COURSE_CODES_ENDPOINT);
        }
        else{
            if(courseList.size() < 1){
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(MainActivity.this);
                }
                builder.setTitle("Enable Internet")
                        .setMessage("Unable to fetch course codes from the server due to no internet connection, enable your internet?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }

        }

        courseCodesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final TextView id = (TextView) view.findViewById(R.id.id);
                final TextView courseCode = (TextView) view.findViewById(R.id.code);
                final TextView courseTitle = (TextView) view.findViewById(R.id.title);
                Intent intent = null;
                String filetype = Constants.PDF_FILE;
                String fileName = filetype + id.getText().toString() + ".pdf";
                intent = new Intent(MainActivity.this, MaterialViewActivity.class);
                intent.putExtra(Constants.KEY_MATERIAL_TYPE, filetype);
                intent.putExtra(Constants.FILE_NAME, fileName);
                intent.putExtra(Constants.KEY_CID, id.getText().toString());
                intent.putExtra(Constants.KEY_CODE, courseCode.getText().toString());
                intent.putExtra(Constants.KEY_TITLE, courseTitle.getText().toString());
                startActivity(intent);
            }
        });

        facebbok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.facebook_handle_url)));
                startActivity(intent);
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.twitter_handle_url)));
                startActivity(intent);
            }
        });

        youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.youtube_handle_url)));
                startActivity(intent);
            }
        });

        //search for course codes
        searchCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                //No result
                Material m = new Material();
                m.setTitle("No result found");
                //get from adapter
                ArrayList<Material> details = getAdapter();
                ArrayList<Material> map = new ArrayList<Material>();
                try {
                    for (int j = 0; j < details.size(); j++) {
                        String val = s.toString().toLowerCase();
                        if (details.get(j).getCode() != null) {
                            if (details.get(j).getCode().toLowerCase().contains(val)) {
                                map.add(details.get(j));
                            }
                        }

                    }
                } catch (NullPointerException ex) {
                    ex.getMessage();
                }
                if (map.size() == 0) {
                    Material material = new Material();
                    material.setCode("No result for your search");
                    material.setTitle("");
                    map.add(material);
                }
                adapter = new CodesCustomList(MainActivity.this, map);
                courseCodesView.setAdapter(adapter);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.chat) {
            // Handle the camera action
            startActivity(new Intent(MainActivity.this, CourseCodesActivity.class));
        } else if (id == R.id.home) {
            startActivity(new Intent(MainActivity.this, MainActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * get course codes adapter
     *
     * @return
     */
    public ArrayList<Material> getAdapter() {
        return courseList;
    }
    /**
    public void getDiscliamer(View view) {
        startActivity(new Intent(MainActivity.this, DisclaimerActivity.class));
    }


     ***/

    /**
     * Download course code
     */
    private class CourseCodeTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            // TODO Auto-generated method stub
            return GET(urls[0]);

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(courseList.size() < 1) {
                showView();
            }
        }
        protected void onPostExecute(String result) {
            hideView();
            Log.d("Result", result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray json = jsonObject.getJSONArray(Constants.CODES_JSON_RESPONSE);
                if(json.length() > 0) {
                    db.deleteCourseCodes(Constants.TABLE_COURSE_CODE);
                    for (int i = 0; i < json.length(); i++) {
                        Material material = new Material();
                        material.setJsonObject(json.getJSONObject(i));
                        db.createOrUpdateCourseCodes(material);
                    }
                    courseList = db.getCourseCodes();
                    adapter = new CodesCustomList(MainActivity.this, courseList);
                    courseCodesView.setAdapter(adapter);
                }
            }
            catch (JSONException ex){
                Toast.makeText(getApplicationContext(), "Error occured", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Get parcel request
     */
    public String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpGet);
            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamtoString(inputStream);
            else
                result = "Did not work!";
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    public String convertInputStreamtoString(InputStream inputStream) throws IOException {
        String result = "";
        String line = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        while ((line = reader.readLine()) != null) {
            result += line;
        }
        reader.close();
        return result;
    }

    /**
     * Show progress bar
     */
    public void showView(){
        pDialog.setMessage(Html.fromHtml("<b>Wait...</b><br/>Fetching course codes."));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        pDialog.show();
    }
    /**
     * Hide Progress
     */
    public void hideView(){
        if(pDialog.isShowing()) {
            pDialog.cancel();
        }
    }
    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onResume(){
        super.onResume();
        if(isConnected()){
            new CourseCodeTask().execute(Constants.COURSE_CODES_ENDPOINT);
        }
    }

}

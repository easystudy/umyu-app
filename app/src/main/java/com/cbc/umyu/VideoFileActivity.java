package com.cbc.umyu;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import custom.Constants;


public class VideoFileActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener ,
        SeekBar.OnSeekBarChangeListener{

    //WEEK SUMMARY VIEWS
    private ValueAnimator timer;
    private int secondsToRun;
    private ListView weekSummary;

    private String study_Title;
    private TextView weekSummaryView;
    private Button goToWeekSummaryButton, goToHalfTermSummaryButton;
    private TextView body_title, body_content, timerCounter, timerValue;
    private ImageView bodyImage;
    private Button next, previous;

    private FrameLayout musicFrame;
    private int mid = 0;

    private int PREVIOUS_PAGE = 0;
    private int CURRENT_PAGE = 0;
    private String dateTimetoMillis;
    private int mYear, mDay, mMonth, mHour, mMinute;
    private ImageView bodyContentImage;
    private ListView multimediaView;
    private MediaPlayer mp;
    // Handler to update UI timer, progress bar etc,.
    private Handler mHandler = new Handler();

    private custom.Utilities utils;
    private int seekForwardTime = 5000; // 5000 milliseconds
    private int seekBackwardTime = 5000; // 5000 milliseconds
    private int currentSongIndex = 0;

    private boolean isShuffle = false;
    private boolean isRepeat = false;
    private Button pop, party, rap;
    private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();

    private ArrayList<HashMap<String, String>> reportObjects = new ArrayList<HashMap<String, String>>();
    ImageButton btnPlay;
    ImageButton btnBackward;
    ImageButton btnForward;
    TextView songCurrentDurationLabel;
    TextView songTotalDurationLabel;
    SeekBar songProgressBar;


    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String SAMPLE_FILE = "sample1.pdf";
    PDFView pdfView;
    Integer pageNumber = 0;
    String videoFileName;
    private EditText text;
    private TextView searched_text;
    private File myInternalFile, myCachedFile, directory;
    private String uriPath;
    private VideoView videoView;
    private android.widget.MediaController mediaController;
    private Uri UrlPath = null;
    private Button status_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_file);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        toolbar.setTitle(getIntent().getStringExtra(Constants.KEY_AVP_TITLE));
        toolbar.setNavigationIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.back_icon, null));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.inflateMenu(R.menu.main);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                //noinspection SimplifiableIfStatement
                if (id == R.id.update) {
                    if(isConnected()) {
                        new DownloadingTask().execute();
                    }
                    else{
                        status_button.setVisibility(View.VISIBLE);
                        status_button.setText("Enable internet to update course material");
                    }
                    return true;
                }
                return onMenuItemClick(item);
            }
        });
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
        songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
        videoView = (VideoView) findViewById(R.id.video);
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        btnForward = (ImageButton) findViewById(R.id.btnForward);
        btnBackward = (ImageButton)findViewById(R.id.btnBackward);

        status_button =(Button) findViewById(R.id.status_button);

        ContextWrapper contextWrapper = new ContextWrapper(VideoFileActivity.this);
        File _directory = contextWrapper.getDir(Constants.filepath, Context.MODE_PRIVATE);
        myCachedFile = new File(_directory , getIntent().getStringExtra(Constants.KEY_AVP_FILE_NAME));

        directory =  new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), Constants.VIDEO_DIRECTORY);
        // Create the storage directory if it does not exist
        if (directory.exists()) {
            myInternalFile = new File(directory.getPath() + File.separator
                    + getIntent().getStringExtra(Constants.KEY_AVP_FILE_NAME));
        }
        //download file if not downloaded
        displayFile();

        songCurrentDurationLabel = (TextView)findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView)findViewById(R.id.songTotalDurationLabel);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
        utils = new custom.Utilities();
        // Listeners
        songProgressBar.setOnSeekBarChangeListener(this); // Important

        Button close = (Button) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoView.stopPlayback();
                finish();
            }
        });
        status_button = (Button) findViewById(R.id.status_button);

        //get file
        status_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isConnected()) {
                    new DownloadingTask().execute();
                }
            }
        });

        btnBackward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (videoView.canSeekBackward()) {
                    // get current song position
                    int currentPosition = videoView.getCurrentPosition();
                    // check if seekBackward time is greater than 0 sec
                    if (currentPosition - seekBackwardTime >= 0) {
                        // forward song
                        videoView.seekTo(currentPosition - seekBackwardTime);
                    } else {
                        // backward to starting position
                        videoView.seekTo(0);
                    }
                }
            }
        });

        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (videoView.canSeekForward()) {
                    // get current song position
                    int currentPosition = videoView.getCurrentPosition();
                    // check if seekForward time is lesser than song duration
                    if (currentPosition + seekForwardTime <= videoView.getDuration()) {
                        // forward song
                        videoView.seekTo(currentPosition + seekForwardTime);
                    } else {
                        // forward to end position
                        videoView.seekTo(videoView.getDuration());
                    }
                }
            }
        });
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myInternalFile.exists() && myInternalFile.isFile()) {
                    if (videoView.isPlaying()) {
                        videoView.pause();
                        // Changing button image to play button
                        btnPlay.setImageResource(R.drawable.img_btn_play);
                    } else {
                        videoView.start();
                        if (videoView.isPlaying()) {
                            btnPlay.setImageResource(R.drawable.btn_pause);
                            songProgressBar.setProgress(0);
                            songProgressBar.setMax(100);
                            updateProgressBar();
                        } else {
                            btnPlay.setImageResource(R.drawable.img_btn_play);
                        }
                    }
                }
                else{
                    status_button.setEnabled(true);
                    status_button.setVisibility(View.VISIBLE);
                    status_button.setText(R.string.download_file);
                    status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.light_green, null));
                    status_button.setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
                }
            }
        });

    }
    @Override
    public void onBackPressed() {
      //  startActivity(new Intent(VideoFileActivity.this, MainActivity.class));
        finish();
    }

    /**
     * Update timer on seekbar
     */
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        btnPlay.setImageResource(R.drawable.img_btn_play);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            videoView.stopPlayback();
        } catch (NullPointerException ex) {
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {}

    /**
     * When user starts moving the progress handler
     * */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
    }
    /**
     * When user stops moving the progress hanlder
     * */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = 0;
        totalDuration = videoView.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);
        videoView.seekTo(currentPosition);
        // update timer progress again
        updateProgressBar();
    }
    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
    private void displayFile() {
        // pdfFileName = assetFileName;
        try {
            if (myInternalFile != null) {
                if (myInternalFile.isFile() && myInternalFile.exists()) {
                    status_button.setVisibility(View.GONE);
                    loadVideo();
                } else {
                    if (isConnected()) {
                        new DownloadingTask().execute();
                    } else {
                        status_button.setVisibility(View.VISIBLE);
                        status_button.setText("Enable internet to download file");
                    }
                }
            }
        }
        catch (NullPointerException ex){ex.printStackTrace();}
    }

    public void loadVideo(){
        try {
            FileInputStream fis = new FileInputStream(myInternalFile);
            FileOutputStream fos = new FileOutputStream(myCachedFile);
            SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, sks);
            int blockSize = cipher.getBlockSize();
            CipherInputStream cis = new CipherInputStream(fis, cipher);
            int b;
            byte[] d = new byte[blockSize * 1024];
            while ((b = cis.read(d)) != -1) {
                fos.write(d, 0, b);
            }
            fos.close();
            cis.close();
            uriPath = myCachedFile.getPath();
            UrlPath = Uri.parse(uriPath);
            mediaController = new android.widget.MediaController(VideoFileActivity.this);
            videoView.setMediaController(mediaController);
            videoView.setVideoURI(UrlPath);
            myCachedFile.deleteOnExit();
        }
        catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }
    /**
     * Background Runnable thread
     * */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            try {
                long totalDuration = 0;
                long currentDuration = 0;
                totalDuration = videoView.getDuration();
                currentDuration = videoView.getCurrentPosition();

                // Displaying Total Duration time
                songTotalDurationLabel.setText("" + utils.milliSecondsToTimer(totalDuration));
                // Displaying time completed playing
                songCurrentDurationLabel.setText("" + utils.milliSecondsToTimer(currentDuration));

                // Updating progress bar
                int progress = (int) (utils.getProgressPercentage(currentDuration, totalDuration));
                //Log.d("Progress", ""+progress);
                songProgressBar.setProgress(progress);
                // Running this thread after 100 milliseconds
                mHandler.postDelayed(this, 100);
            }catch (IllegalStateException ex){}
        }
    };

    //dpownload file
    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            status_button.setEnabled(false);
            status_button.setVisibility(View.VISIBLE);
            status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.yellow, null));
            status_button.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
            status_button.setText(R.string.downloadStarted);//Set Button Text when download started
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (myInternalFile.exists() && myInternalFile.isFile()) {
                    status_button.setEnabled(true);
                    status_button.setVisibility(View.GONE);
                    status_button.setText(R.string.downloadCompleted);//If Download completed then change button tex
                    finish();
                    startActivity(getIntent());
                } else {
                    status_button.setText(R.string.downloadFailed);//If download failed change button text
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            status_button.setEnabled(true);
                            status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.ColorPrimaryDark, null));
                            status_button.setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
                            status_button.setVisibility(View.VISIBLE);
                            status_button.setText(R.string.downloadAgain);//Change button text again after 3sec

                        }
                    }, 3000);
                    Log.e(TAG, "Download Failed");
                }
            } catch (Exception e) {
                e.printStackTrace();

                //Change button text if exception occurs
                status_button.setText(R.string.downloadFailed);
                status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.ColorPrimaryDark, null));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        status_button.setEnabled(true);
                        status_button.setVisibility(View.VISIBLE);

                        status_button.setText(R.string.downloadAgain);
                    }
                }, 3000);
                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }
            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(getIntent().getStringExtra(Constants.KEY_AVP_FILE_URL));//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection
                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());
                }
                if (!myInternalFile.exists()) {
                    myInternalFile.createNewFile();
                    Log.e(TAG, "File Created");
                }
                FileOutputStream fos = new FileOutputStream(myInternalFile);
//                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location
                InputStream is = c.getInputStream();//Get InputStream for connection
                SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
                // Create cipher
                Cipher cipher = Cipher.getInstance("AES");
                int blockSize = cipher.getBlockSize();
                cipher.init(Cipher.ENCRYPT_MODE, sks);
                // Wrap the output stream
                CipherOutputStream cos = new CipherOutputStream(fos, cipher);
                // Write bytes
                byte[] buffer = new byte[blockSize * 1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    cos.write(buffer, 0, len1);//Write new file
                }
                // Flush and close streams.
                cos.flush();
                cos.close();
                fos.close();
            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                myInternalFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }
            return null;
        }
    }
}

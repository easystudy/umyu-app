package com.cbc.umyu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import custom.AVPMaterial;
import custom.Constants;
import custom.CourseCode;
import custom.MaterialsCustomList;
import db.DBController;

public class AvpListActivity extends AppCompatActivity

        implements NavigationView.OnNavigationItemSelectedListener {
    private ListView courseMaterialsView;
    private EditText searchTitle;
    private ArrayList<AVPMaterial> courseList;
    private MaterialsCustomList adapter;
    private ProgressBar progressBar;
    private ArrayList<CourseCode> courseCodes = new ArrayList<CourseCode>();
    private DBController db;
    private SharedPreferences mSharedPrefs;
    private ProgressDialog pDialog;
    private String fileType;
    private TextView errorText;
    private String course_codes_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avp_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        toolbar.setTitle(getIntent().getStringExtra(Constants.KEY_CODE)+": "+getIntent().getStringExtra(Constants.KEY_TITLE));
        toolbar.setNavigationIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.back_icon, null));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        courseList = new ArrayList<AVPMaterial>();
        //get course codes
        searchTitle = (EditText) findViewById(R.id.search_title);
        courseMaterialsView = (ListView) findViewById(R.id.course_materials);
        mSharedPrefs = getSharedPreferences(Constants.CHAT_PREFS, MODE_PRIVATE);
        pDialog = new ProgressDialog(AvpListActivity.this);
        db = new DBController(this);
        fileType = getIntent().getStringExtra(Constants.KEY_MATERIAL_TYPE);
        errorText = (TextView) findViewById(R.id.errorText);
        course_codes_id = getIntent().getStringExtra(Constants.KEY_COURSE_ID);
        courseList = db.getAVPMaterials(getIntent().getStringExtra(Constants.TABLE_NAME), getIntent().getStringExtra(Constants.KEY_COURSE_ID));
        adapter = new MaterialsCustomList(AvpListActivity.this, courseList);
        courseMaterialsView.setAdapter(adapter);
        if(isConnected()){
            new CourseMaterialsTask().execute(Constants.COURSE_MATERIALS_ENDPOINT);
        }
        else{
            if(courseList.size() < 1){
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(AvpListActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(AvpListActivity.this);
                }
                builder.setTitle("Enable Internet")
                        .setMessage("Unable to fetch "+getIntent().getStringExtra(Constants.KEY_MATERIAL_TYPE)+" course codes for "+getIntent().getStringExtra(Constants.KEY_CODE) + " from the server due to no internet connection, enable your internet?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }
        courseMaterialsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final TextView id = (TextView) view.findViewById(R.id.id);
                final TextView courseTitle = (TextView) view.findViewById(R.id.title);
                final TextView fileUrl = (TextView) view.findViewById(R.id.file_url);
                final TextView fileName = (TextView) view.findViewById(R.id.file_name);
                final TextView course_materials_id = (TextView) view.findViewById(R.id.course_materials_id);
                //set material type
                Intent intent = null;
                if(getIntent().getStringExtra(Constants.KEY_MATERIAL_TYPE).equals(Constants.AUDIO)){
                    intent = new Intent(AvpListActivity.this, AudioFileActivity.class);
                    intent.putExtra(Constants.KEY_AVP_FILE_URL, fileUrl.getText().toString());
                    intent.putExtra(Constants.KEY_AVP_COURSE_MATERIALS_ID, course_materials_id.getText().toString());
                    intent.putExtra(Constants.KEY_AVP_TITLE, courseTitle.getText().toString());
                    intent.putExtra(Constants.KEY_AVP_FILE_NAME, fileName.getText().toString());
                }
                else if(getIntent().getStringExtra(Constants.KEY_MATERIAL_TYPE).equals(Constants.VIDEO)){
                    intent = new Intent(AvpListActivity.this, VideoFileActivity.class);
                    intent.putExtra(Constants.KEY_AVP_FILE_URL, fileUrl.getText().toString());
                    intent.putExtra(Constants.KEY_AVP_COURSE_MATERIALS_ID, course_materials_id.getText().toString());
                    intent.putExtra(Constants.KEY_AVP_TITLE, courseTitle.getText().toString());
                    intent.putExtra(Constants.KEY_AVP_FILE_NAME, fileName.getText().toString());
                }
                else if(getIntent().getStringExtra(Constants.KEY_MATERIAL_TYPE).equals(Constants.POWER_POINT)){
                    intent = new Intent(AvpListActivity.this, PowerpointActivity.class);
                    intent.putExtra(Constants.KEY_AVP_FILE_URL, fileUrl.getText().toString());
                    intent.putExtra(Constants.KEY_AVP_COURSE_MATERIALS_ID, course_materials_id.getText().toString());
                    intent.putExtra(Constants.KEY_AVP_TITLE, courseTitle.getText().toString());
                    intent.putExtra(Constants.KEY_AVP_FILE_NAME, fileName.getText().toString());
                }
                else{}
                startActivity(intent);

            }
        });
        //search for course codes
        searchTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                //No result
                AVPMaterial m = new AVPMaterial();
                m.setTitle("No result found");
                //get from adapter
                ArrayList<AVPMaterial> details = getAdapter();
                ArrayList<AVPMaterial> map = new ArrayList<AVPMaterial>();
                try {
                    for (int j = 0; j < details.size(); j++) {
                        String val = s.toString().toLowerCase();
                        if (details.get(j).getTitle() != null) {
                            if (details.get(j).getTitle().toLowerCase().contains(val)) {
                                map.add(details.get(j));
                            }
                        }

                    }
                } catch (NullPointerException ex) {
                    ex.getMessage();
                }
                if (map.size() == 0) {
                    AVPMaterial material = new AVPMaterial();
                    material.setTitle("No result for your search");
                    material.setTitle("");
                    map.add(material);
                }
                adapter = new MaterialsCustomList(AvpListActivity.this, map);
                courseMaterialsView.setAdapter(adapter);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    public void onBackPressed() {
       finish();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.chat) {
            // Handle the camera action
            startActivity(new Intent(AvpListActivity.this, CourseCodesActivity.class));
        } else if (id == R.id.home) {
            startActivity(new Intent(AvpListActivity.this, MainActivity.class));
        }
//        } else if (id == R.id.logout) {
//            SharedPreferences.Editor edit = mSharedPrefs.edit();
//            edit.remove(Constants.CHAT_USERNAME);
//            edit.apply();
//            startActivity(new Intent(MainActivity.this, LoginActivity.class));
//            finish();
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * get course codes adapter
     *
     * @return
     */
    public ArrayList<AVPMaterial> getAdapter() {
        return courseList;
    }



    /**
     * Download course code
     */
    private class CourseMaterialsTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            // TODO Auto-generated method stub
            return GET(urls[0]);

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(courseList.size() < 1) {
                errorText.setVisibility(View.GONE);
                showView();
            }
        }
        protected void onPostExecute(String result) {
            hideView();
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray json = jsonObject.getJSONArray(Constants.MATERIALS_JSON_RESPONSE);
                String status = jsonObject.getString(Constants.STATUS);
                String message = jsonObject.getString(Constants.MESSAGE);
                if(status.equals("200")) {
                    errorText.setVisibility(View.GONE);
                    if (json.length() > 0) {
//                        if (fileType.equals(Constants.AUDIO)) {
//                            db.deleteCourseCodes(Constants.TABLE_AUDIO);
//                        } else if (fileType.equals(Constants.VIDEO)) {
//                            db.deleteCourseCodes(Constants.TABLE_VIDEO);
//                        } else if (fileType.equals(Constants.POWER_POINT)) {
//                            db.deleteCourseCodes(Constants.TABLE_POWER_POINT);
//                        }
                        Log.d("Result", result);
                        for (int i = 0; i < json.length(); i++) {
                            AVPMaterial material = new AVPMaterial();
                            material.setJsonObject(json.getJSONObject(i));
                            if (fileType.equals(Constants.AUDIO)) {
                                db.createOrUpdateAudioMaterials(material, course_codes_id);
                            } else if (fileType.equals(Constants.VIDEO)) {
                                db.createOrUpdateVideoMaterials(material, course_codes_id);
                            } else if (fileType.equals(Constants.POWER_POINT)) {
                                db.createOrUpdatePowerPointMaterials(material, course_codes_id);
                            } else {
                            }
                        }
                        courseList = db.getAVPMaterials(getIntent().getStringExtra(Constants.TABLE_NAME), course_codes_id);
                        adapter = new MaterialsCustomList(AvpListActivity.this, courseList);
                        courseMaterialsView.setAdapter(adapter);
                    }
                    else{
                        errorText.setVisibility(View.VISIBLE);
                        errorText.setText(message);
                    }
                }
                else{
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText(message);
                }
            }
            catch (JSONException ex){
                Toast.makeText(getApplicationContext(), "Error occured", Toast.LENGTH_SHORT).show();
            }
        }
    }
    /**
     * Get parcel request
     */
    public String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url+getIntent().getStringExtra(Constants.KEY_COURSE_ID)+"/"+getIntent().getStringExtra(Constants.KEY_MATERIAL_TYPE));
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpGet);
            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamtoString(inputStream);
            else
                result = "Did not work!";
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }
    public String convertInputStreamtoString(InputStream inputStream) throws IOException {
        String result = "";
        String line = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        while ((line = reader.readLine()) != null) {
            result += line;
        }
        reader.close();
        return result;
    }

    /**
     * Show progress bar
     */
    public void showView(){
        pDialog.setMessage(Html.fromHtml("<b>Wait...</b><br/>Fetching course codes."));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        pDialog.show();
    }
    /**
     * Hide Progress
     */
    public void hideView(){
        if(pDialog.isShowing()) {
            pDialog.cancel();
        }
    }
    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onResume(){
        super.onResume();
        if(isConnected()){
            new CourseMaterialsTask().execute(Constants.COURSE_MATERIALS_ENDPOINT);
        }
    }

}

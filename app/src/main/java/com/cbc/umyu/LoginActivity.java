package com.cbc.umyu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import custom.Constants;

public class LoginActivity extends AppCompatActivity {

    private EditText mUsername;
    private EditText mPin;
    private ProgressDialog pDialog;
    private String pin, name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mUsername = (EditText) findViewById(R.id.login_username);
        mPin = (EditText) findViewById(R.id.login_pin);
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            String lastUsername = extras.getString("oldUsername", "");
            mUsername.setText(lastUsername);
        }
    }
    /**
     * Takes the username from the EditText, check its validity and saves it if valid.
     *   Then, redirects to the MainActivity.
     * @param view Button clicked to trigger call to joinChat
     */
    public void getAccess(View view){
        name = mUsername.getText().toString().trim();
        pin = mPin.getText().toString().trim();
        if (!validateInput(name, pin))
            return;
        if(isConnected()){
            new LoginTask().execute(Constants.USER_ACTIVATION_ENDPOINT);
        }
        else{
            Toast.makeText(getApplicationContext(), getString(R.string.connect_to_internet), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Optional function to specify what a username in your chat app can look like.
     * @param username The name entered by a user.
     * @return
     */
    private boolean validateInput(String username, String pin) {
        if (username.length() == 0) {
            mUsername.setError("Username cannot be empty.");
            return false;
        }
        if (username.length() > 16) {
            mUsername.setError("Username too long.");
            return false;
        }
        if(pin.length() == 0){
            mPin.setError("Pin cannot be empty");
            return  false;
        }
        if(pin.length() > 9){
            mPin.setError("PIN cannot be greater than 9 digits");
            return false;
        }
        return true;
    }


    /**
     * Activate User AsyncTask
     */
    private class LoginTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            // TODO Auto-generated method stub
            try {
                return POST(urls[0]);
            } catch (JSONException ex) {
            }
            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage(Html.fromHtml("<b>Wait...</b><br/>Validating PIN."));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.getWindow().setBackgroundDrawableResource(R.color.textBoxTextColor);
            pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            pDialog.show();
        }
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONObject json = jsonObject.getJSONObject(Constants.APP_USER_JSON_RESPONSE);
                String message =jsonObject.getString(Constants.MESSAGE);
                int status  =   jsonObject.getInt(Constants.STATUS);
                if(status == 200){
                    SharedPreferences sp = getSharedPreferences(Constants.CHAT_PREFS,MODE_PRIVATE);
                    SharedPreferences.Editor edit = sp.edit();
                    edit.putString(Constants.CHAT_USERNAME, name);
                    edit.putString(Constants.ACCESS_PIN, pin);
                    edit.apply();
                    //Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    Intent intent = new Intent(LoginActivity.this, FacultyActivity.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }
            }
            catch (JSONException ex){
                Toast.makeText(getApplicationContext(), getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
            }
        }
    }
    /**
     * Post user data to the server
     */
    public String POST(String url) throws JSONException{
        InputStream inputStream = null;
        String result = "";
        JSONObject params;
        params = new JSONObject();
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            params.put(Constants.USER_NAME, name);
            params.put(Constants.USER_PIN, pin);
            json = params.toString();
            StringEntity se = new StringEntity(json);
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpClient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null) {
                result = convertInputStreamtoString(inputStream);
            } else
                result = "Did not work";
        } catch (Exception e) {
        }
        return result;
    }
    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;

    }
    /**
     * Convert input to stream
     * @param inputStream
     * @return
     * @throws IOException
     */
    public String convertInputStreamtoString(InputStream inputStream) throws IOException {
        String result = "";
        String line = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        while ((line = reader.readLine()) != null) {
            result += line;
        }
        reader.close();
        return result;
    }
}

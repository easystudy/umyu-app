package com.cbc.umyu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import custom.FacultyList;
import custom.Constants;
import custom.Material;
import db.DBController;

public class FacultyActivity extends AppCompatActivity {
    private ListView courseCodesView;
    private EditText searchCode;
   // private ArrayList<String> courseList = new ArrayList<String>();
   ArrayList<String> facullty = new ArrayList<>();
    private FacultyList adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        toolbar.setTitle("Select Faculty");
        toolbar.setNavigationIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.back_icon, null));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //db = new DBController(this);
        searchCode =  findViewById(R.id.search_code);
        courseCodesView =  findViewById(R.id.course_codes);
       //courseList = db.getCourseCodes();
        //facullty.add("Applied Science");
        facullty.add("Natural and Applied Sciences");
        adapter = new FacultyList(FacultyActivity.this, facullty);
        courseCodesView.setAdapter(adapter);
        courseCodesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                Intent intent = new Intent(FacultyActivity.this, DepartmentActivity.class);
                //intent.putExtra(Constants.KEY_CODE, courseCode.getText().toString());
                //intent.putExtra(Constants.KEY_TITLE, courseTitle.getText().toString());
                startActivity(intent);
                finish();
            }});

        //search for course codes
        /**searchCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                //No result
                Material m = new Material();
                m.setTitle("No result found");
                //get from adapter
                ArrayList<Material> details = getAdapter();
                ArrayList<Material> map=new ArrayList<Material>();
                try {
                    for (int j = 0; j < details.size(); j++) {
                        String val = s.toString().toLowerCase();
                        if(details.get(j).getCode()!=null) {
                            if (details.get(j).getCode().toLowerCase().contains(val)) {
                                map.add(details.get(j));
                            }
                        }

                    }
                }catch (NullPointerException ex){
                    ex.getMessage();
                }
                if(map.size() == 0){
                    Material material = new Material();
                    material.setCode("No result for your search");
                    material.setTitle("");
                    map.add(material);
                }
                adapter =new FacultyList(FacultyActivity.this, map);
                courseCodesView.setAdapter(adapter);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });**/
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    /**
     * get course codes adapter
     * @return
     */
    public ArrayList<String> getAdapter(){
        return facullty;
    }
}

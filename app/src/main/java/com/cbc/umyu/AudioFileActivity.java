package com.cbc.umyu;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import custom.Constants;

public class AudioFileActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener{
    private static final String TAG = MainActivity.class.getSimpleName();
    //WEEK SUMMARY VIEWS
    private Button status_button;
    private MediaPlayer mp;
    // Handler to update UI timer, progress bar etc,.
    private Handler mHandler = new Handler();

    private custom.Utilities utils;
    private int seekForwardTime = 5000; // 5000 milliseconds
    private int seekBackwardTime = 5000; // 5000 milliseconds

    ImageButton btnPlay;
    ImageButton btnBackward;
    ImageButton btnForward;
    TextView songCurrentDurationLabel;
    TextView songTotalDurationLabel;
    SeekBar songProgressBar;


    private File myInternalFile, myCachedFile, directory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_file);
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        toolbar.setTitle(getIntent().getStringExtra(Constants.KEY_AVP_TITLE));
        toolbar.setNavigationIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.back_icon, null));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.inflateMenu(R.menu.main);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.update) {
                    if(isConnected()) {
                        new DownloadingTask().execute();
                    }
                    else{
                        status_button.setVisibility(View.VISIBLE);
                        status_button.setText("Enable internet to update course material");
                    }
                    return true;
                }
                return onMenuItemClick(item);
            }
        });
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
        songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        btnBackward = (ImageButton)findViewById(R.id.btnBackward);
        btnForward = (ImageButton) findViewById(R.id.btnForward);
        status_button = (Button) findViewById(R.id.status_button);
        songCurrentDurationLabel = (TextView)findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);

        status_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isConnected()) {
                    new DownloadingTask().execute();
                }
            }
        });
        utils = new custom.Utilities();
        // Listeners
        mp = new MediaPlayer();
        songProgressBar.setOnSeekBarChangeListener(AudioFileActivity.this); // Important
        // mp.setOnCompletionListener(this); // Important

        ContextWrapper contextWrapper = new ContextWrapper(AudioFileActivity.this);
        File _directory = contextWrapper.getDir(Constants.filepath, Context.MODE_PRIVATE);
        myCachedFile = new File(_directory , getIntent().getStringExtra(Constants.KEY_AVP_FILE_NAME));

        directory =  new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), Constants.AUDIO_DIRECTORY);
        // Create the storage directory if it does not exist
        if (directory.exists()) {
            myInternalFile = new File(directory.getPath() + File.separator
                    + getIntent().getStringExtra(Constants.KEY_AVP_FILE_NAME));
        }
        //download file if not downloaded
        displayFile();

        Button close = (Button) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.release();
                finish();
            }
        });

        /**
         * Play button click event
         * plays a song and changes button to pause image
         * pauses a song and changes button to play image
         * */

        btnPlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // check for already playing
                try {
                    if(myInternalFile.isFile() && myInternalFile.exists()) {
                        if (mp.isPlaying()) {
                            if (mp != null) {
                                mp.pause();
                                // Changing button image to play button
                                btnPlay.setImageResource(R.drawable.img_btn_play);
                            } else {
                                btnPlay.setImageResource(R.drawable.img_btn_play);
                            }
                        } else {
                            // Resume song
                            if (mp != null) {
                                mp.reset();
                                loadAudio();
                                mp.start();
                                // Changing button image to pause button
                                btnPlay.setImageResource(R.drawable.btn_pause);
                                songProgressBar.setProgress(0);
                                songProgressBar.setMax(100);
                                updateProgressBar();
                            } else {
                                btnPlay.setImageResource(R.drawable.img_btn_play);
                            }
                        }
                    }
                    else{
                        status_button.setEnabled(true);
                        status_button.setVisibility(View.VISIBLE);
                        status_button.setText(R.string.download_file);
                        status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.light_green, null));
                        status_button.setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                catch (NullPointerException ex){}

            }
        });

        /**
         * Forward button click event
         * Forwards song specified seconds
         * */
        btnForward.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // get current song position
                int currentPosition = mp.getCurrentPosition();
                // check if seekForward time is lesser than song duration
                if (currentPosition + seekForwardTime <= mp.getDuration()) {
                    // forward song
                    mp.seekTo(currentPosition + seekForwardTime);
                } else {
                    // forward to end position
                    mp.seekTo(mp.getDuration());
                }
            }
        });

        /**
         * Backward button click event
         * Backward song to specified seconds
         * */
        btnBackward.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // get current song position
                int currentPosition = mp.getCurrentPosition();
                // check if seekBackward time is greater than 0 sec
                if (currentPosition - seekBackwardTime >= 0) {
                    // forward song
                    mp.seekTo(currentPosition - seekBackwardTime);
                } else {
                    // backward to starting position
                    mp.seekTo(0);
                }
            }
        });
    }

    /**
     * set study material
     */

    @Override
    public void onBackPressed() {
        //startActivity(new Intent(AudioFileActivity.this, MainActivity.class));
        finish();
    }

    public String getWeekSummaryValue(int value) {
        String weekSummary = "";
        if (value <= 5) {
            weekSummary = "1ST HALF";
        } else {
            weekSummary = "2ND HALF";
        }
        return weekSummary;
    }

    /**
     * Function to play a song
     */
    public void playSong(AssetFileDescriptor afd) {
        // Play song
        try {
            mp.reset();
            mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mp.prepare();
            mp.start();
            // Changing Button Image to pause image
            btnPlay.setImageResource(R.drawable.btn_pause);
            // set Progress bar values
            songProgressBar.setProgress(0);
            songProgressBar.setMax(100);

            // Updating progress bar
            updateProgressBar();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update timer on seekbar
     */
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }



    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        btnPlay.setImageResource(R.drawable.img_btn_play);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mp.release();
        } catch (NullPointerException ex) {
        }
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {}

    /**
     * When user starts moving the progress handler
     * */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
    }
    /**
     * When user stops moving the progress hanlder
     * */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = 0;
        totalDuration = mp.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);
        // forward or backward to certain seconds

        mp.seekTo(currentPosition);
        // update timer progress again
        updateProgressBar();
    }

    /**
     * Background Runnable thread
     * */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            try {
                long totalDuration = 0;
                long currentDuration = 0;
                totalDuration = mp.getDuration();
                currentDuration = mp.getCurrentPosition();
                // Displaying Total Duration time
                songTotalDurationLabel.setText("" + utils.milliSecondsToTimer(totalDuration));
                // Displaying time completed playing
                songCurrentDurationLabel.setText("" + utils.milliSecondsToTimer(currentDuration));

                // Updating progress bar
                int progress = (int) (utils.getProgressPercentage(currentDuration, totalDuration));
                //Log.d("Progress", ""+progress);
                songProgressBar.setProgress(progress);
                // Running this thread after 100 milliseconds
                mHandler.postDelayed(this, 100);
            }catch (IllegalStateException ex){}
        }
    };


    private void displayFile() {
        try {
            if(myInternalFile != null) {
                if (myInternalFile.isFile() && myInternalFile.exists()) {
                    status_button.setVisibility(View.GONE);
                    loadAudio();
                } else {
                    if (isConnected()) {
                        new DownloadingTask().execute();
                    } else {
                        status_button.setVisibility(View.VISIBLE);
                        status_button.setText("Enable internet to download file");
                    }
                }
            }
        }
        catch (NullPointerException ex) {ex.printStackTrace();}
    }

    /**
     * load song from app internal storage
     */
    public void loadAudio(){
        try {
            FileInputStream fis = new FileInputStream(myInternalFile);
            FileOutputStream fos = new FileOutputStream(myCachedFile);
            SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, sks);
            int blockSize = cipher.getBlockSize();
            CipherInputStream cis = new CipherInputStream(fis, cipher);
            int b;
            byte[] d = new byte[blockSize * 1024];
            while((b = cis.read(d)) != -1) {
                fos.write(d, 0, b);
            }
            fos.flush();
            fos.close();
            cis.close();
            mp.setDataSource(myCachedFile.getPath());
            mp.prepare();
            myCachedFile.deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (NullPointerException ex){}
        catch (NoSuchPaddingException ex){}
        catch (NoSuchAlgorithmException ex){}
        catch (InvalidKeyException ex){}
    }


    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;

    }



    //dpownload file
    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            status_button.setEnabled(false);
            status_button.setVisibility(View.VISIBLE);
            status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.yellow, null));
            status_button.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
            status_button.setText(R.string.downloadStarted);//Set Button Text when download started
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (myInternalFile.exists() && myInternalFile.isFile()) {
                    status_button.setEnabled(true);
                    status_button.setVisibility(View.GONE);
                    status_button.setText(R.string.downloadCompleted);//If Download completed then change button tex
                    finish();
                    startActivity(getIntent());
                } else {
                    status_button.setText(R.string.downloadFailed);//If download failed change button text
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            status_button.setEnabled(true);
                            status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.ColorPrimaryDark, null));
                            status_button.setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
                            status_button.setVisibility(View.VISIBLE);
                            status_button.setText(R.string.downloadAgain);//Change button text again after 3sec

                        }
                    }, 3000);
                    Log.e(TAG, "Download Failed");
                }
            } catch (Exception e) {
                e.printStackTrace();

                //Change button text if exception occurs
                status_button.setText(R.string.downloadFailed);
                status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.ColorPrimaryDark, null));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        status_button.setEnabled(true);
                        status_button.setVisibility(View.VISIBLE);
                        status_button.setText(R.string.downloadAgain);
                    }
                }, 3000);
                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }
            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                //URL url = new URL( Constants.downloadUrl + Constants.VIDEOPATH + getIntent().getStringExtra(Constants.FILE_NAME));//Create Download URl
                URL url = new URL(getIntent().getStringExtra(Constants.KEY_AVP_FILE_URL));
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection
                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());
                }
                if (!myInternalFile.exists()) {
                    myInternalFile.createNewFile();
                }
                FileOutputStream fos = new FileOutputStream(myInternalFile);
//                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location
                InputStream is = c.getInputStream();//Get InputStream for connection
                SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
                // Create cipher
                Cipher cipher = Cipher.getInstance("AES");
                int blockSize = cipher.getBlockSize();
                cipher.init(Cipher.ENCRYPT_MODE, sks);
                // Wrap the output stream
                CipherOutputStream cos = new CipherOutputStream(fos, cipher);
                // Write bytes
                byte[] buffer = new byte[blockSize * 1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    cos.write(buffer, 0, len1);//Write new file
                }
                // Flush and close streams.
                cos.flush();
                cos.close();
                fos.close();
            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                myInternalFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }
            return null;
        }
    }
}

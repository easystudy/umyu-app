package com.cbc.umyu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import custom.Constants;
import db.DBController;

public class DisclaimerActivity extends AppCompatActivity {
    private DBController db;
    private SharedPreferences mSharedPrefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disclaimer);
        db = new DBController(this);
        mSharedPrefs = getSharedPreferences(Constants.CHAT_PREFS, MODE_PRIVATE);
        if (!mSharedPrefs.contains(Constants.CHAT_USERNAME)){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent toLogin = new Intent(DisclaimerActivity.this, LoginActivity.class);
                    startActivity(toLogin);
                    finish();
                }
            }, 190000);
        }
        else{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(DisclaimerActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 190000);
        }

    }
}
package com.cbc.umyu;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.shockwave.pdfium.PdfDocument;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;

import custom.Constants;
import custom.DownloadTask;

public class MaterialViewActivity extends AppCompatActivity  implements OnPageChangeListener,OnLoadCompleteListener {

    private DownloadTask downloadTask;
    private WebView wv;
    private Button status_button;


    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String SAMPLE_FILE = "sample1.pdf";
    PDFView pdfView;
    Integer pageNumber = 0;
    String pdfFileName;
    private EditText text;
    private TextView searched_text;
    private File myInternalFile, myCachedFile, directory;
    private ArrayList<View> views = new ArrayList<View>();
    private Button audio, video, powerpoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_view);
        pdfView = (PDFView)findViewById(R.id.pdfView);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        toolbar.setTitle(getIntent().getStringExtra(Constants.KEY_CODE)+": "+getIntent().getStringExtra(Constants.KEY_TITLE));
        toolbar.setNavigationIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.back_icon, null));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.inflateMenu(R.menu.main);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                //noinspection SimplifiableIfStatement
                if (id == R.id.update) {
                    if(isConnected()) {
                        new DownloadingTask().execute();
                    }
                    else{
                        status_button.setVisibility(View.VISIBLE);
                        status_button.setText("Enable internet to update course material");
                    }
                    return true;
                }
                return onMenuItemClick(item);
            }
        });
        status_button = (Button) findViewById(R.id.status_button);
        text = (EditText) findViewById(R.id.search);
        views.add(searched_text);

        audio = (Button) findViewById(R.id.audio);
        video = (Button) findViewById(R.id.video);
        powerpoint = (Button) findViewById(R.id.power_point);
        //navigate to Avp Listview to view list of course materials attached to the selected course
        audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MaterialViewActivity.this, AvpListActivity.class);
                intent.putExtra(Constants.KEY_MATERIAL_TYPE, Constants.AUDIO);
                intent.putExtra(Constants.KEY_COURSE_ID, getIntent().getStringExtra(Constants.KEY_CID));
                intent.putExtra(Constants.KEY_CODE, getIntent().getStringExtra(Constants.KEY_CODE));
                intent.putExtra(Constants.KEY_TITLE, getIntent().getStringExtra(Constants.KEY_TITLE));
                intent.putExtra(Constants.TABLE_NAME, Constants.TABLE_AUDIO);
                startActivity(intent);
            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MaterialViewActivity.this, AvpListActivity.class);
                intent.putExtra(Constants.KEY_MATERIAL_TYPE, Constants.VIDEO);
                intent.putExtra(Constants.KEY_CODE, getIntent().getStringExtra(Constants.KEY_CODE));
                intent.putExtra(Constants.KEY_TITLE, getIntent().getStringExtra(Constants.KEY_TITLE));
                intent.putExtra(Constants.KEY_COURSE_ID, getIntent().getStringExtra(Constants.KEY_CID));
                intent.putExtra(Constants.TABLE_NAME, Constants.TABLE_VIDEO);
                startActivity(intent);
            }
        });

        powerpoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MaterialViewActivity.this, AvpListActivity.class);
                intent.putExtra(Constants.KEY_MATERIAL_TYPE, Constants.POWER_POINT);
                intent.putExtra(Constants.KEY_CODE, getIntent().getStringExtra(Constants.KEY_CODE));
                intent.putExtra(Constants.KEY_TITLE, getIntent().getStringExtra(Constants.KEY_TITLE));
                intent.putExtra(Constants.KEY_COURSE_ID, getIntent().getStringExtra(Constants.KEY_CID));
                intent.putExtra(Constants.TABLE_NAME, Constants.TABLE_POWER_POINT);
                startActivity(intent);
            }
        });

        status_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isConnected()) {
                    new DownloadingTask().execute();
                }
            }
        });

        ContextWrapper contextWrapper = new ContextWrapper(MaterialViewActivity.this);
        File _directory = contextWrapper.getDir(Constants.filepath, Context.MODE_PRIVATE);
        myCachedFile = new File(_directory , getIntent().getStringExtra(Constants.KEY_AVP_FILE_NAME));

        directory =  new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), Constants.PDF_DIRECTORY);
        // Create the storage directory if it does not exist
    //    if (directory.exists()) {
            myInternalFile = new File(directory.getPath() + File.separator
                    + getIntent().getStringExtra(Constants.KEY_AVP_FILE_NAME));
        //}
        //download file if not downloaded
        displayFile();
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pdfView.findViewsWithText(views, charSequence, i);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

      //  String doc = "<iframe src='http://docs.google.com/viewer?url=http://www.oracle.com/events/global/en/java-outreach/resources/java-a-beginners-guide-1720064.pdf&embedded=true' width='100%' height='100%'style='border: none; display='none';'></iframe>";
    }
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }


    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");
        pdfView.getViewTreeObserver();
    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    private void loadPage(){
        try {
            pdfView.fromFile(myCachedFile)
                    .defaultPage(pageNumber)
                    .enableSwipe(true)
                    .swipeHorizontal(false)
                    .onPageChange(this)
                    .enableAnnotationRendering(true)
                    .onLoad(this)
                    .scrollHandle(new DefaultScrollHandle(this))
                    .load();
        }
        catch (Exception ex){}
    }
    private void displayFile() {
        if (myInternalFile != null) {
            if (myInternalFile.isFile() && myInternalFile.exists()) {
                Log.e("Open", "Decrypted file opened");
                try {
                    SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
                    Cipher cipher = Cipher.getInstance("AES");
                    cipher.init(Cipher.DECRYPT_MODE, sks);
                    int blockSize = cipher.getBlockSize();
                    int outputSize = cipher.getOutputSize(blockSize);
                    System.out.println("outputsize: " + outputSize);
                    byte[] inBytes = new byte[blockSize * 1024]; //modified
                    byte[] outBytes = new byte[outputSize * 1024]; //modified
                    FileInputStream in = new FileInputStream(myInternalFile);
                    FileOutputStream out = new FileOutputStream(myCachedFile);
                    BufferedInputStream inStream = new BufferedInputStream(in);
                    int inLength = 0;
                    ;
                    boolean more = true;
                    while (more) {
                        inLength = inStream.read(inBytes);
                        if (inLength / 1024 == blockSize) //modified
                        {
                            int outLength = cipher.update(inBytes, 0, blockSize * 1024, outBytes);//modified
                            out.write(outBytes, 0, outLength);
                        } else more = false;
                    }
                    if (inLength > 0)
                        outBytes = cipher.doFinal(inBytes, 0, inLength);
                    else
                        outBytes = cipher.doFinal();
                    out.write(outBytes);
                    loadPage();

                } catch (FileNotFoundException ex) {
                } catch (NoSuchPaddingException e) {
                    Log.e("Error1", e.getMessage());
                    e.printStackTrace();

                } catch (NoSuchAlgorithmException e) {
                    Log.e("Error2", e.getMessage());
                    e.printStackTrace();

                } catch (IOException e) {
                    Log.e("Error3", e.getMessage());
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    Log.e("Error4", e.getMessage());
                    e.printStackTrace();
                } catch (ShortBufferException ex) {
                } catch (BadPaddingException ex) {
                } catch (IllegalBlockSizeException ex) {
                }
            } else {
                Log.e("FileNot", "File not found");
                if (isConnected()) {
                    new DownloadingTask().execute();
                } else {
                    status_button.setVisibility(View.VISIBLE);
                    status_button.setText("Enable internet to download file");
                }
            }
        }
    }
    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;

    }

    //dpownload file
    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            status_button.setEnabled(false);
            status_button.setVisibility(View.VISIBLE);
            status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.yellow, null));
            status_button.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
            status_button.setText(R.string.downloadStarted);//Set Button Text when download started
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (myInternalFile.exists() && myInternalFile.isFile()) {
                    status_button.setEnabled(true);
                    status_button.setVisibility(View.GONE);
                    status_button.setText(R.string.downloadCompleted);//If Download completed then change button tex
                    finish();
                    startActivity(getIntent());
                } else {
                    status_button.setText(R.string.downloadFailed);//If download failed change button text
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            status_button.setEnabled(true);
                            status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.ColorPrimaryDark, null));
                            status_button.setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
                            status_button.setVisibility(View.VISIBLE);
                            status_button.setText(R.string.downloadAgain);//Change button text again after 3sec

                        }
                    }, 3000);
                    Log.e(TAG, "Download Failed");
                }
            } catch (Exception e) {
                e.printStackTrace();

                //Change button text if exception occurs
                status_button.setText(R.string.downloadFailed);
                status_button.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.ColorPrimaryDark, null));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        status_button.setEnabled(true);
                        status_button.setVisibility(View.VISIBLE);

                        status_button.setText(R.string.downloadAgain);
                    }
                }, 3000);
                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }
            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL( Constants.downloadUrl + Constants.PDFPATH + getIntent().getStringExtra(Constants.FILE_NAME));//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection
                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());
                }
               // if(myInternalFile != null) {
                    if (!myInternalFile.exists()) {
                        myInternalFile.createNewFile();
                        Log.e(TAG, "File Created");
                    }
               // }
                FileOutputStream fos = new FileOutputStream(myInternalFile);
//                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location
                InputStream is = c.getInputStream();//Get InputStream for connection
                SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
                // Create cipher
                Cipher cipher = Cipher.getInstance("AES");
                int blockSize = cipher.getBlockSize();
                cipher.init(Cipher.ENCRYPT_MODE, sks);
                // Wrap the output stream
                CipherOutputStream cos = new CipherOutputStream(fos, cipher);
                // Write bytes
                byte[] buffer = new byte[blockSize * 1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    cos.write(buffer, 0, len1);//Write new file
                }
                // Flush and close streams.
                cos.flush();
                cos.close();
                fos.close();

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                myInternalFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }
            return null;
        }
    }
}
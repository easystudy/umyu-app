package db;
/**
 * @author JOSEPH
 *
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import custom.AVPMaterial;
import custom.Constants;
import custom.Material;

public class DBController extends SQLiteOpenHelper{
	private SQLiteDatabase dbase;
	public DBController(Context applicationcontext) {
        super(applicationcontext, "easy_study.db", null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase database) {
		dbase = database;
        String course_code, audioMaterial, videoMaterial, powerPointMaterial;
        course_code = "CREATE TABLE IF NOT EXISTS course_codes (id INTEGER PRIMARY KEY AUTOINCREMENT, cid TEXT, code TEXT, title TEXT)";
		audioMaterial = "CREATE TABLE IF NOT EXISTS audio_materials (id INTEGER PRIMARY KEY AUTOINCREMENT, aid TEXT, title TEXT, file_name TEXT, file_url TEXT, course_materials_id TEXT, users_id TEXT, course_codes_id TEXT)";
		videoMaterial = "CREATE TABLE IF NOT EXISTS video_materials (id INTEGER PRIMARY KEY AUTOINCREMENT, vid TEXT, title TEXT, file_name TEXT, file_url TEXT, course_materials_id TEXT, users_id TEXT, course_codes_id TEXT)";
		powerPointMaterial = "CREATE TABLE IF NOT EXISTS power_point_materials (id INTEGER PRIMARY KEY AUTOINCREMENT, pid TEXT, title TEXT, file_name TEXT, file_url TEXT, course_materials_id TEXT, users_id TEXT, course_codes_id TEXT)";
        dbase.execSQL(course_code);
		dbase.execSQL(audioMaterial);
		dbase.execSQL(videoMaterial);
		dbase.execSQL(powerPointMaterial);
	//	addCodes();
    }
    @Override
    public void onUpgrade(SQLiteDatabase database, int version_old, int current_version) {
        String query1;
        query1 = "DROP TABLE IF EXISTS "+Constants.TABLE_COURSE_CODE;
		//database.execSQL(query1);
        onCreate(database);
    }

	/**
	 * Create of update Customer's data
	 * @param material
	 * @throws JSONException
     */
    public void createOrUpdateCourseCodes(Material material) throws JSONException{
    	dbase=this.getWritableDatabase();
		try{
			String sql="SELECT * FROM "+ Constants.TABLE_COURSE_CODE+" WHERE code = '"+material.getJCode()+"'";
			Cursor cursor=dbase.rawQuery(sql, null);
			if(cursor.getCount()>0){
				ContentValues val=new ContentValues();
				val.put(Constants.KEY_CID, material.getJId());
				val.put(Constants.KEY_CODE, material.getJCode());
				val.put(Constants.KEY_TITLE, material.getJTitle());
				dbase.update(Constants.TABLE_COURSE_CODE,val," code = " + material.getJCode(),null);
				dbase.close();
			}
			else{
				ContentValues val=new ContentValues();
				val.put(Constants.KEY_CID, material.getJId());
				val.put(Constants.KEY_CODE, material.getJCode());
				val.put(Constants.KEY_TITLE, material.getJTitle());
				dbase.insert(Constants.TABLE_COURSE_CODE, null, val);
				dbase.close();
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
		}
	}


	/**
	 * Create of update Customer's data
	 * @param material
	 * @throws JSONException
	 */
	public void createOrUpdateAudioMaterials(AVPMaterial material, String course_codes_id) throws JSONException{
		dbase=this.getWritableDatabase();
		try{
			String sql="SELECT * FROM "+ Constants.TABLE_AUDIO+" WHERE aid = '"+material.getId()+"'";
			Cursor cursor=dbase.rawQuery(sql, null);
			if(cursor.getCount()>0){
				ContentValues val=new ContentValues();
				val.put(Constants.KEY_AID, material.getId());
				val.put(Constants.KEY_AVP_TITLE, material.getJ_title());
				val.put(Constants.KEY_AVP_FILE_NAME, material.getJ_fileName());
				val.put(Constants.KEY_AVP_FILE_URL, material.getJ_fileUrl());
				val.put(Constants.KEY_AVP_COURSE_MATERIALS_ID, material.getJ_courseMaterialId());
				val.put(Constants.KEY_AVP_USERS_ID, material.getJ_userId());
				val.put(Constants.KEY_AVP_COURSE_CODES_ID, course_codes_id);
				dbase.update(Constants.TABLE_AUDIO,val," aid = " + material.getId(),null);
				dbase.close();
			}
			else{
				ContentValues val=new ContentValues();
				val.put(Constants.KEY_AID, material.getId());
				val.put(Constants.KEY_AVP_TITLE, material.getJ_title());
				val.put(Constants.KEY_AVP_FILE_NAME, material.getJ_fileName());
				val.put(Constants.KEY_AVP_FILE_URL, material.getJ_fileUrl());
				val.put(Constants.KEY_AVP_COURSE_MATERIALS_ID, material.getJ_courseMaterialId());
				val.put(Constants.KEY_AVP_USERS_ID, material.getJ_userId());
				val.put(Constants.KEY_AVP_COURSE_CODES_ID, course_codes_id);
				dbase.insert(Constants.TABLE_AUDIO, null, val);
				dbase.close();
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
		}
	}



	/**
	 * Create of update Customer's data
	 * @param material
	 * @throws JSONException
	 */
	public void createOrUpdateVideoMaterials(AVPMaterial material, String course_codes_id) throws JSONException{
		dbase=this.getWritableDatabase();
		try{
			String sql="SELECT * FROM "+ Constants.TABLE_VIDEO+" WHERE vid = '"+material.getId()+"'";
			Cursor cursor=dbase.rawQuery(sql, null);
			if(cursor.getCount()>0){
				ContentValues val=new ContentValues();
				val.put(Constants.KEY_VID, material.getId());
				val.put(Constants.KEY_AVP_TITLE, material.getJ_title());
				val.put(Constants.KEY_AVP_FILE_NAME, material.getJ_fileName());
				val.put(Constants.KEY_AVP_FILE_URL, material.getJ_fileUrl());
				val.put(Constants.KEY_AVP_COURSE_MATERIALS_ID, material.getJ_courseMaterialId());
				val.put(Constants.KEY_AVP_USERS_ID, material.getJ_userId());
				val.put(Constants.KEY_AVP_COURSE_CODES_ID, course_codes_id);
				dbase.update(Constants.TABLE_VIDEO,val," vid = " + material.getId(),null);
				dbase.close();
			}
			else{
				ContentValues val=new ContentValues();
				val.put(Constants.KEY_VID, material.getId());
				val.put(Constants.KEY_AVP_TITLE, material.getJ_title());
				val.put(Constants.KEY_AVP_FILE_NAME, material.getJ_fileName());
				val.put(Constants.KEY_AVP_FILE_URL, material.getJ_fileUrl());
				val.put(Constants.KEY_AVP_COURSE_MATERIALS_ID, material.getJ_courseMaterialId());
				val.put(Constants.KEY_AVP_USERS_ID, material.getJ_userId());
				val.put(Constants.KEY_AVP_COURSE_CODES_ID, course_codes_id);
				dbase.insert(Constants.TABLE_VIDEO, null, val);
				dbase.close();
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Create of update Customer's data
	 * @param material
	 * @throws JSONException
	 */
	public void createOrUpdatePowerPointMaterials(AVPMaterial material, String course_codes_id) throws JSONException{
		dbase=this.getWritableDatabase();
		try{
			String sql="SELECT * FROM "+ Constants.TABLE_POWER_POINT+" WHERE pid = '"+material.getId()+"'";
			Cursor cursor=dbase.rawQuery(sql, null);
			if(cursor.getCount()>0){
				ContentValues val=new ContentValues();
				val.put(Constants.KEY_PID, material.getId());
				val.put(Constants.KEY_AVP_TITLE, material.getJ_title());
				val.put(Constants.KEY_AVP_FILE_NAME, material.getJ_fileName());
				val.put(Constants.KEY_AVP_FILE_URL, material.getJ_fileUrl());
				val.put(Constants.KEY_AVP_COURSE_MATERIALS_ID, material.getJ_courseMaterialId());
				val.put(Constants.KEY_AVP_USERS_ID, material.getJ_userId());
				val.put(Constants.KEY_AVP_COURSE_CODES_ID, course_codes_id);
				dbase.update(Constants.TABLE_POWER_POINT,val," vid = " + material.getId(),null);
				dbase.close();
			}
			else{
				ContentValues val=new ContentValues();
				val.put(Constants.KEY_PID, material.getId());
				val.put(Constants.KEY_AVP_TITLE, material.getJ_title());
				val.put(Constants.KEY_AVP_FILE_NAME, material.getJ_fileName());
				val.put(Constants.KEY_AVP_FILE_URL, material.getJ_fileUrl());
				val.put(Constants.KEY_AVP_COURSE_MATERIALS_ID, material.getJ_courseMaterialId());
				val.put(Constants.KEY_AVP_USERS_ID, material.getJ_userId());
				val.put(Constants.KEY_AVP_COURSE_CODES_ID, course_codes_id);
				dbase.insert(Constants.TABLE_POWER_POINT, null, val);
				dbase.close();
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
		}
	}


	//get audio, video or power point materials
	public ArrayList<AVPMaterial> getAVPMaterials(String tableName, String course_codes_id){
		dbase	=	this.getReadableDatabase();
		String sql="SELECT * FROM "+ tableName+" WHERE course_codes_id = '"+course_codes_id+"'";
		Cursor c =  dbase.rawQuery(sql, null);
		ArrayList<AVPMaterial> materials = new ArrayList<AVPMaterial>();
		if(c.moveToFirst()){
			do{
				AVPMaterial material = new AVPMaterial();
				material.setTitle(c.getString(2));
				material.setFileName(c.getString(3));
				material.setFileUrl(c.getString(4));
				material.setCourseMaterialId(c.getString(5));
				material.setUserId(c.getString(6));
				materials.add(material);
			}while(c.moveToNext());
		}
		return materials;
	}

	public void deleteCourseCodes(String tableName) {
		dbase.execSQL("delete from " + tableName);
	}

	public ArrayList<Material> getCourseCodes(){
		dbase	=	this.getReadableDatabase();
		String sql = "select * from "+Constants.TABLE_COURSE_CODE;
		Cursor c =  dbase.rawQuery(sql, null);
		ArrayList<Material> materials = new ArrayList<Material>();
		if(c.moveToFirst()){
			do{
				Material material = new Material();
				material.setId(c.getString(1));
				material.setCode(c.getString(2));
				material.setTitle(c.getString(3));
				materials.add(material);
			}while(c.moveToNext());
		}
		return materials;
	}

	/**
	 * Create default course codes before fetching from the server
	 */
	private void addCodes()
	{
		Material c1=new Material("1","ACC 111", "Introduction to Accounting");
		this.addCode(c1);

		Material  m2 = new Material("2","BFN 122", "Quantitative Methods II");
		this.addCode(m2);

		Material m3 = new Material( "3","BUS 111", "Introduction to Business");
		this.addCode(m3);

		Material m4 = new Material("4","ECO 111", "Principles of Economics I (Macro)");
		this.addCode(m4);

		Material m5 = new Material("5","ECO 121", "MicroEconomics");
		this.addCode(m5);

		Material m6= new Material("6","ENT 121", "Introduction to Entrepreneurship");
		this.addCode(m6);

		Material m7= new Material("7","HRM 121", "Introduction to Behavior Sciences");
		this.addCode(m7);

	}
	// Adding new question
	public void addCode(Material material) {
		ContentValues values = new ContentValues();
		values.put(Constants.KEY_CID, material.getCode());
		values.put(Constants.KEY_CODE, material.getCode());
		values.put(Constants.KEY_TITLE, material.getTitle());
		dbase.insert(Constants.TABLE_COURSE_CODE, null, values);
	}
}
